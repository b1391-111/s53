let collection = [];

// Write the queue functions below.
const print = () => {
    return [];
}

const enqueue = (name) => {
    collection = [...collection, name];
    return collection;
}


const dequeue = () => {
    collection = collection.filter((item, index) => index !== 0)
    return collection;
}


const front = () => {
    return collection[0];
}

const size = () => {
    return collection.length;
}

const isEmpty = () => {
    return collection === 0;
}



module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};